## progetto ejb-glassfish
E' un progetto ejb semplice, dove al momento del deploy il singleton esegue il metodo *init* che e annotato con *@Postconstruct* e stampa dei messagi sul log del server. 
Controlla il log dopo ogni deploy.
Anche se il progetto si chiama 'ejb-glassfish' alla fine l'ho eseguito su wildfly perchè con il glassfish ho avuto dei problemi e poi non lo conosco molto bene. 
Per eseguire questi esempi è meglio se usi un application server wildfly. 

### build instruction 
 - mvn clean install
 - dopo che cambi il branch fai sempre un 'Maven update' or 'alt+F5' per aggiornare il progetto
 
### info branches
Fai checkout in ordine: 
 - 01 -> fa import della libreria 'javax' che fa parte della librerie Java EE. Per queste librerie non devi fare nulla per le dipendenze, perche sono riconosciute e importate in automatico dai application server Java EE compliant. 
 - 02 -> faccio uso della libreria 'com.google.guava'. Perchè nel file descriptor (MANIFEST.MF) non viene speficata la dipendenza al runtime del modulo guava ( che è presente nel server wildfly come modulo ) il progetto scoppia al deploy. 
 - 03 -> qui dichiaro nel MANIFEST.MF la dipendenza del modulo 'com.google.guava' quindi lo trova al runtime e il deploy avviene correttamente 
 - 04 -> in questo esempio uso un plugin che oltre il jar normale crea un jar che contiente anche le dipendenze, mettendo dentro la libreria 'guava'. Apri entrambi jar e vedi le diferenze. Il 'jar-with-dependencies' contiente anche la libreria guava.
