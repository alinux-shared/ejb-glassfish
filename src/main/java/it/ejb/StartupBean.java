package it.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Startup
@Singleton
public class StartupBean {

    private String status;

    @PostConstruct
    public void init() {

        status = "Ejb StartupBean created .... ";

        System.out.println(status);
        System.out.println();
        System.out.println(" --- ");

    }
}
